package com.neo.util;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 添加配置
 *
 * @author: zhangchen
 * When: 2019/6/11 10:26
 **/
@Component
@Data
public class NeoProperties {

    @Value("${com.neo.title}")
    private String title;

    @Value("${com.neo.description}")
    private String description;

}
