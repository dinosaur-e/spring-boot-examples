package com.neo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1，简单的http对外连接
 * 2，端口号是8080
 *
 * @author: zhangchen
 * When: 2019/6/6 10:25
 **/
@SpringBootApplication
public class HelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloApplication.class, args);
	}
}
